---------------------------------------------------------------
-- 
-- Project Name: 	Lab 1	
-- Module Name: 	adder_tb - testbench	
-- Create Date: 	07:09:45 01/16/2014 
-- Description: 	A testbench to test the functionality of the
--	             	full adder module over all possible inputs
--
---------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
ENTITY adder_tb IS
END adder_tb;
 
ARCHITECTURE behavior OF adder_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT adder
    PORT(
         A : IN  std_logic;
         B : IN  std_logic;
         Cin : IN  std_logic;
         S : OUT  std_logic;
         Cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic := '0';
   signal B : std_logic := '0';
   signal Cin : std_logic := '0';

   --Outputs
   signal S : std_logic;
   signal Cout : std_logic;

 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: adder PORT MAP (
          A => A,
          B => B,
          Cin => Cin,
          S => S,
          Cout => Cout
        );

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

		-- Simulate every possible input to the adder of 3 inputs
		-- A, B and CarryIn (Cin)
		A <= '0';
		B <= '0';
		Cin <= '0';
		wait for 10ns;
		A <= '0';
		B <= '0';
		Cin <= '1';
		wait for 10ns; 
		A <= '0';
		B <= '1';
		Cin <= '0';
		wait for 10ns;
		A <= '0';
		B <= '1';
		Cin <= '1';
		wait for 10ns; 
		A <= '1';
		B <= '0';
		Cin <= '0';
		wait for 10ns;
		A <= '1';
		B <= '0';
		Cin <= '1';
		wait for 10ns; 
		A <= '1';
		B <= '1';
		Cin <= '0';
		wait for 10ns;
		A <= '1';
		B <= '1';
		Cin <= '1';
		wait for 10ns; 
      
		-- Stop the function repeating like a clock by awiting forever
      wait;
   end process;

END;
