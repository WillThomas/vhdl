---------------------------------------------------------------
-- 
-- Project Name: 	Lab 1
-- Module Name: 	adder - Behavioral 		
-- Create Date: 	07:06:12 01/16/2014 
-- Description: 	A full adder module for a single bit
--
---------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Define the external structure of the adder (Inputs/Outputs)
entity adder is
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC;
           Cout : out  STD_LOGIC);
end adder;


-- Describe the functionality of the adder
architecture Behavioral of adder is
signal P,G,Cprop: STD_LOGIC;
begin
	G <= A and B;
	P <= A xor B;
	Cprop <= P and Cin;
	Cout <= Cprop or G;
	S <= P xor Cin;
end Behavioral; 


