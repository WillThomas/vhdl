\contentsline {section}{\numberline {0.1}Implementation of components}{2}
\contentsline {subsection}{\numberline {0.1.1}VHDL code for adder}{2}
\contentsline {subsection}{\numberline {0.1.2}VHDL code for testbench}{3}
\contentsline {subsection}{\numberline {0.1.3}Simulation screenshot}{5}
\contentsline {section}{\numberline {0.2}Implementation of sequential elements}{7}
\contentsline {subsection}{\numberline {0.2.1}VHDL for Circuit}{7}
\contentsline {subsection}{\numberline {0.2.2}VHDL for testbench}{9}
\contentsline {subsection}{\numberline {0.2.3}Screenshots of simulation}{11}
\contentsline {subsection}{\numberline {0.2.4}HDL synthesis}{13}
\contentsline {section}{\numberline {0.3}Design of a 7 segment display controller}{14}
\contentsline {subsection}{\numberline {0.3.1}VHDL circuit code}{14}
\contentsline {subsubsection}{BCD counter (00-99)}{16}
\contentsline {subsubsection}{Debouncer}{18}
\contentsline {subsubsection}{7 Segment Display Driver}{19}
\contentsline {subsection}{\numberline {0.3.2}VHDL Testbenches code}{20}
\contentsline {subsection}{\numberline {0.3.3}Screenshots of Testbenches}{22}
\contentsline {subsubsection}{Initialization}{22}
\contentsline {subsubsection}{09-10 Carry}{22}
\contentsline {subsubsection}{99-100 Carry}{23}
\contentsline {subsubsection}{System Reset}{23}
\contentsline {subsection}{\numberline {0.3.4}HDL Synthesis Report}{24}
\contentsline {subsection}{\numberline {0.3.5}Deboucer Explanation}{26}
\contentsline {subsubsection}{Circuit diagram}{26}
\contentsline {subsubsection}{Debouncer testbench}{26}
