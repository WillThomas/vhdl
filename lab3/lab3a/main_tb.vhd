
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
USE ieee.numeric_std.ALL;
 
ENTITY main_tb IS
END main_tb;
 
ARCHITECTURE behavior OF main_tb IS 
  
    COMPONENT main
    PORT(
         CLK : IN  std_logic;
         RST : IN  std_logic;
         Qout : OUT  std_logic_vector(7 downto 0);
         count : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RST : std_logic := '0';
   signal count : std_logic := '0';

 	--Outputs
   signal Qout : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          CLK => CLK,
          RST => RST,
          Qout => Qout,
          count => count
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
   button_process :process
   begin
		count <= '0';
		wait for CLK_period*3;
		count <= '1';
		wait for CLK_period*3;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		RST <= '1';
      wait for CLK_period * 3;
		RST <= '0';
		wait for 500 ns;	
		RST <= '1';
		wait for CLK_period;
		RST <= '0';

      -- insert stimulus here 

      wait;
   end process;

END;
