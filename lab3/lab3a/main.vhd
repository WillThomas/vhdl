library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity main is
    Port ( CLK : in  STD_LOGIC;
			  RST: in STD_LOGIC;
           Qout : out  std_logic_vector (7 downto 0);
           count : in  STD_LOGIC);
end main;

architecture arch of main is
	-- define internal count register. Note  type is unsigned as required 
	-- to use + operand o to increase count
	signal Qint : UNSIGNED(3 downto 0);
	signal deb_count : STD_LOGIC;
	signal deb_rst: STD_LOGIC;
		

COMPONENT debouncer
	PORT(
		clock : IN std_logic;
		Ain : IN std_logic;          
		Output : OUT std_logic
		);
	END COMPONENT;
	
COMPONENT rom
  PORT (
    a : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
    spo : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
  );
END COMPONENT;

begin


-- counter process
counter: process
begin
	wait until (rising_edge(CLK));
	
	if (RST = '1') then
		Qint <= "0000";
	else
		if (deb_count = '1') then
			if (Qint = 14) then 
				Qint <= "0000";
			else
				Qint <= (Qint + 1);
			end if;
		end if;
	end if;
	
end process counter;


-- declare rom interface
fib_rom : rom
	PORT MAP (a=> std_logic_vector(Qint), spo => Qout);
	
Rst_debouncer: debouncer 
	PORT MAP(
		clock => CLK, Ain => RST, Output => deb_rst);
En_debouncer: debouncer 
	PORT MAP(
		clock => CLK, Ain => count, Output => deb_count);

end arch;

