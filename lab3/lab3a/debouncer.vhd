----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:16:58 01/31/2014 
-- Design Name: 
-- Module Name:    debouncer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
    Port ( clock : in  STD_LOGIC;
           Ain : in  STD_LOGIC;
           Output : out  STD_LOGIC);
end debouncer;

architecture Behavioral of debouncer is
	
	-- debouncer internal registers
	signal dbc : std_logic_vector (2 downto 0);
	
begin

-- Debouncer process
debouncer: process (clock) 

begin
	if (clock'event and clock = '1') then
		dbc(0) <= Ain;
		dbc(1) <= dbc(0);
		dbc(2) <= dbc(1);
	end if;
end process debouncer;

-- configure debounced count
Output <= dbc(0) and dbc(1) and (not dbc(2));

end Behavioral;



