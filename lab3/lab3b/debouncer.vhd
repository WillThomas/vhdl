
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity debouncer is
    Port ( clock : in  STD_LOGIC;
           Ain : in  STD_LOGIC;
           Output : out  STD_LOGIC);
end debouncer;


-- This debouncer outputs high when finds "110" in last 3 inputs at clock edges
-- most recent left
architecture Behavioral of debouncer is
	
	-- debouncer internal register(flipflops)
	signal dbc : std_logic_vector (2 downto 0);
	
begin

-- Debouncer process
debouncer: process (clock) 

begin
	if (clock'event and clock = '1') then
		dbc(0) <= Ain;
		dbc(1) <= dbc(0);
		dbc(2) <= dbc(1);
	end if;
end process debouncer;

-- set the output to ensure last values were 011
Output <= dbc(0) and dbc(1) and (not dbc(2));

end Behavioral;