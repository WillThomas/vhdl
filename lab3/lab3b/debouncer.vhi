
-- VHDL Instantiation Created from source file debouncer.vhd -- 17:52:45 01/31/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT debouncer
	PORT(
		clock : IN std_logic;
		Ain : IN std_logic;          
		Output : OUT std_logic
		);
	END COMPONENT;

	Inst_debouncer: debouncer PORT MAP(
		clock => ,
		Ain => ,
		Output => 
	);


