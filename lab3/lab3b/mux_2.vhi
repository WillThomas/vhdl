
-- VHDL Instantiation Created from source file mux_2.vhd -- 17:34:26 01/31/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT mux_2
	PORT(
		sel : IN std_logic_vector(1 downto 0);
		Ain : IN std_logic_vector(15 downto 0);          
		Qout : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_mux_2: mux_2 PORT MAP(
		sel => ,
		Ain => ,
		Qout => 
	);


