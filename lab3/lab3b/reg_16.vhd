
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg_16 is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           en : in  STD_LOGIC;
           Ain : in  STD_LOGIC_VECTOR (15 downto 0);
           Qout : out  STD_LOGIC_VECTOR (15 downto 0));
end reg_16;

architecture Behavioral of reg_16 is
begin

--register process
reg_16 : process
begin
	wait until (rising_edge(clk));

	if (rst = '1') then
		Qout <= (others => '0');
	else
		if (en = '1') then	
			Qout <= Ain;
		end if;
	end if;
	
end process reg_16;

end Behavioral;

