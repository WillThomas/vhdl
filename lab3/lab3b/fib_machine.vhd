library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


entity fib_machine is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           nxt : in  STD_LOGIC;
           fib_out : out  STD_LOGIC_VECTOR (15 downto 0));
end fib_machine;


architecture Behavioral of fib_machine is

-- control signals
signal en_out, en_r1, en_r2, mem_wr : STD_LOGIC;
signal mem_addr : STD_LOGIC_VECTOR (4 downto 0);
signal mux_sel : STD_LOGIC_VECTOR(1 downto 0);


-- internal pathways o_< component name>
signal o_ram, o_r1, o_r2, o_mux, in_mux : STD_LOGIC_VECTOR(15 downto 0);
signal deb_rst, deb_nxt: STD_LOGIC;

COMPONENT fib_ram
  PORT (
    a : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    d : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    clk : IN STD_LOGIC;
    we : IN STD_LOGIC;
    spo : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

COMPONENT reg_16
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		en : IN std_logic;
		Ain : IN std_logic_vector(15 downto 0);          
		Qout : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;
	
COMPONENT mux_2
	PORT(
		sel : IN std_logic_vector(1 downto 0);
		Ain : IN std_logic_vector(15 downto 0);          
		Qout : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;

COMPONENT state_machine_controller
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		nxt : IN std_logic;          
		en_r1 : OUT std_logic;
		en_r2 : OUT std_logic;
		en_write : OUT std_logic;
		en_out : OUT std_logic;
		mux_sel : OUT std_logic_vector(1 downto 0);
		mem_addr : OUT std_logic_vector(4 downto 0)
		);
		
END COMPONENT;

COMPONENT debouncer
	PORT(
		clock : IN std_logic;
		Ain : IN std_logic;          
		Output : OUT std_logic
		);
END COMPONENT;

begin
in_mux <= std_logic_vector(unsigned(o_r1) + unsigned(o_r2));
 
reg_1: reg_16 PORT MAP(
		clk => clk,
		rst => deb_rst,
		en => en_r1,
		Ain => o_ram,
		Qout => o_r1 
	);
	
reg_2: reg_16 PORT MAP(
		clk => clk,
		rst => deb_rst,
		en => en_r2,
		Ain => o_ram,
		Qout => o_r2
	);

reg_3: reg_16 PORT MAP(
		clk => clk,
		rst => deb_rst,
		en => en_out,
		Ain => o_mux,
		Qout => fib_out
	);

mux: mux_2 PORT MAP(
		sel => mux_sel,
		Ain => in_mux,
		Qout => o_mux
	);

logic_control: state_machine_controller PORT MAP(
		clk => clk,
		rst => deb_rst,
		nxt => deb_nxt,
		en_r1 => en_r1,
		en_r2 => en_r2,
		en_write => mem_wr,
		en_out => en_out,
		mux_sel => mux_sel,
		mem_addr => mem_addr 
	);
	
debouncer_rst: debouncer PORT MAP(
		clock => clk,
		Ain => rst,
		Output => deb_rst
	);
	
debouncer_next: debouncer PORT MAP(
		clock => clk,
		Ain => nxt,
		Output => deb_nxt
	);
	
	
ram : fib_ram
  PORT MAP (
    a => mem_addr,
    d => o_mux,
    clk => clk,
    we => mem_wr,
    spo => o_ram
  );
	
end Behavioral;








