
-- VHDL Instantiation Created from source file reg_16.vhd -- 17:33:21 01/31/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT reg_16
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		en : IN std_logic;
		Ain : IN std_logic_vector(15 downto 0);          
		Qout : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_reg_16: reg_16 PORT MAP(
		clk => ,
		rst => ,
		en => ,
		Ain => ,
		Qout => 
	);


