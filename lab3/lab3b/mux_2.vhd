
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_2 is
    Port ( sel : in  STD_LOGIC_VECTOR (1 downto 0);
           Ain : in  STD_LOGIC_VECTOR (15 downto 0);
           Qout : out  STD_LOGIC_VECTOR (15 downto 0));
end mux_2;

architecture Behavioral of mux_2 is

begin

Qout <= 	(others => '0') when sel = "00" else
			"0000000000000001" when sel = "01" else
			Ain when sel = "10" else
			(others => 'U');

end Behavioral;

