
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity state_machine_controller is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           nxt : in  STD_LOGIC;
           en_r1 : out  STD_LOGIC;
           en_r2 : out  STD_LOGIC;
           en_write : out  STD_LOGIC;
           en_out : out  STD_LOGIC;
           mux_sel : out  STD_LOGIC_VECTOR (1 downto 0);
           mem_addr : out  STD_LOGIC_VECTOR (4 downto 0));
end state_machine_controller;

architecture arch of state_machine_controller is


	type states is (s0,s1,s2,s3,s4);
	signal state: states;
	signal done : STD_LOGIC;
	signal mem_addr_diff: integer;
	signal mem_addr_int: unsigned(4 downto 0);
begin


en_r1 <= '1' when state = s2 else '0'; --d
en_r2 <='1' when state = s3 else '0'; --d
en_write <= '1' when (state = s0 or state = s1  or state = s4) else '0';
en_out <= '1' when (state = s0 or state = s1  or state = s4) else '0';
mux_sel <= "00" when state = s0 else
				"01" when state = s1 else "10";

mem_addr_diff <= 	2 when (state = s2) else
						1 when ((state = s3)) else
						0;


mem_addr <= std_logic_vector(mem_addr_int - mem_addr_diff);


addr_count: process (clk) is
begin
--neecd counter proc in here
--count to 24 sync reset enable on state = s3
if rising_edge(clk) then
	if (rst = '1') then
		mem_addr_int <= (others => '0');
		done <= '0';
	elsif
	-- this couldpotentially be proken by push button 2 times very fast inc counter butoy incs in s0 1 4 so its ok,
		((state = s4) or (state = s0) or (state = s1)) then
			if (mem_addr_int < 24) then
				if( nxt = '1') then
					mem_addr_int <= mem_addr_int +1 ;
				end if;
			else
				done <= '1';
			end if;
	end if;

end if;

end process addr_count;

next_state: process(clk) is
begin
	if rising_edge(clk) then
		if(rst = '1') then
			state<=s0;
			
		else
			if (done = '0') then  --check if we are finished, if we are then  no more state changes til reset pressed
				case state is
					when  s0 => if(nxt = '1') then state <=s1;end if;
					when  s1 => if(nxt = '1') then state <=s2; end if;
					when  s2 =>state <=s3;
					when  s3 =>state <=s4;
					when  s4 => if(nxt = '1') then		--this holds state until button pushed
										state <=s2;
									end if;
				end case;
			end if;
		end if;
	end if;
end process next_state;

--set outputs here;



end arch;

