
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY fibonacci_tb IS
END fibonacci_tb;
 
ARCHITECTURE behavior OF fibonacci_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fib_machine
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         nxt : IN  std_logic;
         fib_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal nxt : std_logic := '0';

 	--Outputs
   signal fib_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fib_machine PORT MAP (
          clk => clk,
          rst => rst,
          nxt => nxt,
          fib_out => fib_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
    button_process :process
   begin
		nxt <= '0';
		wait for clk_period*15;
		nxt <= '1';
		wait for clk_period*3;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 180 ns;	
		rst <= '1';
      wait for clk_period*3;
		rst <= '0';
      wait for clk_period*3;

      -- insert stimulus here 

      wait;
   end process;

END;
