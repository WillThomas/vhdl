
-- VHDL Instantiation Created from source file state_machine_controller.vhd -- 17:35:25 01/31/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT state_machine_controller
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		nxt : IN std_logic;          
		en_r1 : OUT std_logic;
		en_r2 : OUT std_logic;
		en_write : OUT std_logic;
		en_out : OUT std_logic;
		mux_sel : OUT std_logic_vector(1 downto 0);
		mem_addr : OUT std_logic_vector(4 downto 0)
		);
	END COMPONENT;

	Inst_state_machine_controller: state_machine_controller PORT MAP(
		clk => ,
		rst => ,
		nxt => ,
		en_r1 => ,
		en_r2 => ,
		en_write => ,
		en_out => ,
		mux_sel => ,
		mem_addr => 
	);


