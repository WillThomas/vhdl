
-- VHDL Instantiation Created from source file fib_machine.vhd -- 17:32:27 01/31/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT fib_machine
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		nxt : IN std_logic;          
		fib_out : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	Inst_fib_machine: fib_machine PORT MAP(
		clk => ,
		rst => ,
		nxt => ,
		fib_out => 
	);


