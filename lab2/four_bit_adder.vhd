library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity four_bit_adder is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           B : in  STD_LOGIC_VECTOR (3 downto 0);
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (3 downto 0);
           Cout : out  STD_LOGIC);
end four_bit_adder;

architecture structural of four_bit_adder is
	COMPONENT adder
	PORT(
		A : IN std_logic;
		B : IN std_logic;
		Cin : IN std_logic;          
		S : OUT std_logic;
		Cout : OUT std_logic
		);
	END COMPONENT;
	signal c1,c2,c3 : std_logic;
begin
	add0: adder
		PORT MAP ( A =>A(0), B => B(0), Cin => Cin , S => S(0), Cout => c1);
	add1: adder
		PORT MAP ( A =>A(1), B => B(1), Cin => c1, S => S(1), Cout => c2);
	add2: adder
		PORT MAP ( A =>A(2), B => B(2), Cin => c2, S => S(2), Cout => c3);
	add3: adder
		PORT MAP ( A =>A(3), B => B(3), Cin => c3, S => S(3), Cout => Cout);

end structural;

