LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY four_bit_adder_tb IS
END four_bit_adder_tb;
 
ARCHITECTURE behavior OF four_bit_adder_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT four_bit_adder
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         Cin : IN  std_logic;
         S : OUT  std_logic_vector(3 downto 0);
         Cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal Cin : std_logic := '0';

 	--Outputs
   signal S : std_logic_vector(3 downto 0);
   signal Cout : std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: four_bit_adder PORT MAP (
          A => A,
          B => B,
          Cin => Cin,
          S => S,
          Cout => Cout
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		
		Cin <= '0';

		--Blank test to ensure stable output with no inputs
		A <= "0000";
		B <= "0000";
		
		-- Testing the 4 internal carries
		wait for 10 ns;
		A <= "0001";
		B <= "0001";

		wait for 10 ns;
		A <= "0010";
		B <= "0010";
		
		wait for 10 ns;
		A <= "0100";
		B <= "0100";

		wait for 10 ns;
		A <= "1000";
		B <= "1000";
		
		-- Reset outputs
		wait for 10 ns;
		Cin <= '0';
		A <= "0000";
		B <= "0000";
		
		-- Test External carry
		wait for 10 ns;
		Cin <= '1';
		A <= "0000";
		B <= "0000";
		
		-- Test first sub adder with external carry and single input
		wait for 10 ns;
		Cin <= '1';
		A <= "0001";
		B <= "0000";
		
		--Test first adder with carry
		wait for 10 ns;
		Cin <= '1';
		A <= "0001";
		B <= "0001";
		
		-- Test behaivour for all carries andoverflow
		wait for 10 ns;
		Cin <= '1';
		A <= "1111";
		B <= "1111";
		
		wait for 10 ns;
		Cin <= '0';
		A <= "0000";
		B <= "0000";
	
      wait;
   end process;

END;
