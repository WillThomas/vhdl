library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Import IEEE lib to allow + and other operations
use IEEE.NUMERIC_STD.ALL;

entity task is
    Port ( clk : in  STD_LOGIC;
           en : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           Out1 : out  UNSIGNED (3 downto 0);
           Out2 : out  UNSIGNED (3 downto 0);
           Out3 : out  UNSIGNED (3 downto 0);
           Out4 : out  UNSIGNED (3 downto 0));
end task;

architecture arch of task is
	--define 4 4 bit busses which act as internal data storage for current state/count
	signal Out1_int : UNSIGNED (3 downto 0);
	signal Out2_int : UNSIGNED (3 downto 0);
	signal Out3_int : UNSIGNED (3 downto 0);
	signal Out4_int : UNSIGNED (3 downto 0);

begin
	count1: process (clk) --Synchronous reset, no enable
		begin
			if(clk'event and clk='1') then
				if(RST = '1') then
					Out1_int <="0000";
				else 
					Out1_int <= (Out1_int +1);
				end if;				
			end if;
	end process count1;
		
	count2: process (clk, rst) --Asynchronous reset, no enable
		begin
		--reset > clock so comes first
		if(rst = '1') then
			Out2_int <= "0000";
		else 
			if(clk'event and clk='1') then
				Out2_int <= (Out2_int +1);								
			end if;
		end if;
	end process count2;
					
	count3: process (clk) --Synchronous reset, with enable
		begin
			if(clk'event and clk='1') then
				if(RST = '1') then
					Out3_int <="0000";
				else
					if(en = '1') then
						Out3_int <= (Out3_int +1);
					end if;
				end if;				
			end if;	
	end process count3;
		
	count4: process (clk, rst) --Asynchronous reset, no enable
		begin
		-- reset > clock so comes first
		if(rst = '1') then
			Out4_int <= "0000";
		else
			if(clk'event and clk='1') then
				if(en = '1') then
					Out4_int <= (Out4_int +1);	
				end if;
			end if;
		end if;
	end process count4;
	
	-- map the outputs to the internal varibles
	Out1 <= Out1_int;
	Out2 <= Out2_int;
	Out3 <= Out3_int;
	Out4 <= Out4_int;

end arch;

