LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY counters_tb IS
END counters_tb;
 
ARCHITECTURE behavior OF counters_tb IS 
 
    COMPONENT task
    PORT(
         clk : IN  std_logic;
         en : IN  std_logic;
         rst : IN  std_logic;
         Out1 : OUT  UNSIGNED(3 downto 0);
         Out2 : OUT  UNSIGNED(3 downto 0);
         Out3 : OUT  UNSIGNED(3 downto 0);
         Out4 : OUT  UNSIGNED(3 downto 0)
        );
    END COMPONENT;
    
   --Inputs
   signal clk : std_logic := '0';
   signal en : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal Out1 : UNSIGNED(3 downto 0);
   signal Out2 : UNSIGNED(3 downto 0);
   signal Out3 : UNSIGNED(3 downto 0);
   signal Out4 : UNSIGNED(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: task PORT MAP (
          clk => clk,
          en => en,
          rst => rst,
          Out1 => Out1,
          Out2 => Out2,
          Out3 => Out3,
          Out4 => Out4
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		--push reset and enable lines high
		rst <= '1';
		en <= '1';
      wait for clk_period*2;
		--drop reset line, allow counters to begin counting
		rst <= '0';
		wait for clk_period*8/2;
		-- reset counters not on a clock rising edge to show
		-- the asynchronous behaviour
		rst <='1';
		wait for clk_period*2;
		rst <='0';
		-- disable counters 3 and 4,counters 1 and 2 should
		-- still be counting after here
		wait for clk_period*4;
		en <='0'; 

		 

      wait;
   end process;

END;
