--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.28xd
--  \   \         Application: netgen
--  /   /         Filename: task_synthesis.vhd
-- /___/   /\     Timestamp: Wed Jan 22 09:17:44 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm task -w -dir netgen/synthesis -ofmt vhdl -sim task.ngc task_synthesis.vhd 
-- Device	: xc6slx45-3-csg324
-- Input file	: task.ngc
-- Output file	: Q:\Xilinx\Labs\Lab2\task2\task2\netgen\synthesis\task_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: task
-- Xilinx	: Q:\Xilinx\14.2\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity task is
  port (
    clk : in STD_LOGIC := 'X'; 
    en : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    Out1 : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    Out2 : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    Out3 : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    Out4 : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end task;

architecture Structure of task is
  signal clk_BUFGP_0 : STD_LOGIC; 
  signal en_IBUF_1 : STD_LOGIC; 
  signal rst_IBUF_2 : STD_LOGIC; 
  signal Result_0_1 : STD_LOGIC; 
  signal Result_1_1 : STD_LOGIC; 
  signal Result_2_1 : STD_LOGIC; 
  signal Result_3_1 : STD_LOGIC; 
  signal Result_0_2 : STD_LOGIC; 
  signal Result_1_2 : STD_LOGIC; 
  signal Result_2_2_13 : STD_LOGIC; 
  signal Result_3_2_14 : STD_LOGIC; 
  signal Result : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Out2_int : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Out3_int : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal Out1_int : STD_LOGIC_VECTOR ( 3 downto 0 ); 
begin
  Out3_int_0 : FDRE
    port map (
      C => clk_BUFGP_0,
      CE => en_IBUF_1,
      D => Result(0),
      R => rst_IBUF_2,
      Q => Out3_int(0)
    );
  Out3_int_1 : FDRE
    port map (
      C => clk_BUFGP_0,
      CE => en_IBUF_1,
      D => Result(1),
      R => rst_IBUF_2,
      Q => Out3_int(1)
    );
  Out3_int_2 : FDRE
    port map (
      C => clk_BUFGP_0,
      CE => en_IBUF_1,
      D => Result(2),
      R => rst_IBUF_2,
      Q => Out3_int(2)
    );
  Out3_int_3 : FDRE
    port map (
      C => clk_BUFGP_0,
      CE => en_IBUF_1,
      D => Result(3),
      R => rst_IBUF_2,
      Q => Out3_int(3)
    );
  Out2_int_0 : FDC
    port map (
      C => clk_BUFGP_0,
      CLR => rst_IBUF_2,
      D => Result_0_2,
      Q => Out2_int(0)
    );
  Out2_int_1 : FDC
    port map (
      C => clk_BUFGP_0,
      CLR => rst_IBUF_2,
      D => Result_1_2,
      Q => Out2_int(1)
    );
  Out2_int_2 : FDC
    port map (
      C => clk_BUFGP_0,
      CLR => rst_IBUF_2,
      D => Result_2_2_13,
      Q => Out2_int(2)
    );
  Out2_int_3 : FDC
    port map (
      C => clk_BUFGP_0,
      CLR => rst_IBUF_2,
      D => Result_3_2_14,
      Q => Out2_int(3)
    );
  Out1_int_0 : FDR
    port map (
      C => clk_BUFGP_0,
      D => Result_0_1,
      R => rst_IBUF_2,
      Q => Out1_int(0)
    );
  Out1_int_1 : FDR
    port map (
      C => clk_BUFGP_0,
      D => Result_1_1,
      R => rst_IBUF_2,
      Q => Out1_int(1)
    );
  Out1_int_2 : FDR
    port map (
      C => clk_BUFGP_0,
      D => Result_2_1,
      R => rst_IBUF_2,
      Q => Out1_int(2)
    );
  Out1_int_3 : FDR
    port map (
      C => clk_BUFGP_0,
      D => Result_3_1,
      R => rst_IBUF_2,
      Q => Out1_int(3)
    );
  Mcount_Out2_int_xor_1_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => Out2_int(1),
      I1 => Out2_int(0),
      O => Result_1_2
    );
  Mcount_Out3_int_xor_1_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => Out3_int(1),
      I1 => Out3_int(0),
      O => Result(1)
    );
  Mcount_Out1_int_xor_1_11 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => Out1_int(1),
      I1 => Out1_int(0),
      O => Result_1_1
    );
  Result_3_21 : LUT4
    generic map(
      INIT => X"6CCC"
    )
    port map (
      I0 => Out2_int(2),
      I1 => Out2_int(3),
      I2 => Out2_int(0),
      I3 => Out2_int(1),
      O => Result_3_2_14
    );
  Result_2_21 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => Out2_int(2),
      I1 => Out2_int(0),
      I2 => Out2_int(1),
      O => Result_2_2_13
    );
  Result_3_2 : LUT4
    generic map(
      INIT => X"6CCC"
    )
    port map (
      I0 => Out3_int(2),
      I1 => Out3_int(3),
      I2 => Out3_int(0),
      I3 => Out3_int(1),
      O => Result(3)
    );
  Result_2_2 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => Out3_int(2),
      I1 => Out3_int(0),
      I2 => Out3_int(1),
      O => Result(2)
    );
  Result_3_11 : LUT4
    generic map(
      INIT => X"6CCC"
    )
    port map (
      I0 => Out1_int(2),
      I1 => Out1_int(3),
      I2 => Out1_int(0),
      I3 => Out1_int(1),
      O => Result_3_1
    );
  Result_2_11 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => Out1_int(2),
      I1 => Out1_int(0),
      I2 => Out1_int(1),
      O => Result_2_1
    );
  en_IBUF : IBUF
    port map (
      I => en,
      O => en_IBUF_1
    );
  rst_IBUF : IBUF
    port map (
      I => rst,
      O => rst_IBUF_2
    );
  Out1_3_OBUF : OBUF
    port map (
      I => Out1_int(3),
      O => Out1(3)
    );
  Out1_2_OBUF : OBUF
    port map (
      I => Out1_int(2),
      O => Out1(2)
    );
  Out1_1_OBUF : OBUF
    port map (
      I => Out1_int(1),
      O => Out1(1)
    );
  Out1_0_OBUF : OBUF
    port map (
      I => Out1_int(0),
      O => Out1(0)
    );
  Out2_3_OBUF : OBUF
    port map (
      I => Out2_int(3),
      O => Out2(3)
    );
  Out2_2_OBUF : OBUF
    port map (
      I => Out2_int(2),
      O => Out2(2)
    );
  Out2_1_OBUF : OBUF
    port map (
      I => Out2_int(1),
      O => Out2(1)
    );
  Out2_0_OBUF : OBUF
    port map (
      I => Out2_int(0),
      O => Out2(0)
    );
  Out3_3_OBUF : OBUF
    port map (
      I => Out3_int(3),
      O => Out3(3)
    );
  Out3_2_OBUF : OBUF
    port map (
      I => Out3_int(2),
      O => Out3(2)
    );
  Out3_1_OBUF : OBUF
    port map (
      I => Out3_int(1),
      O => Out3(1)
    );
  Out3_0_OBUF : OBUF
    port map (
      I => Out3_int(0),
      O => Out3(0)
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_0
    );
  Mcount_Out2_int_xor_0_11_INV_0 : INV
    port map (
      I => Out2_int(0),
      O => Result_0_2
    );
  Mcount_Out3_int_xor_0_11_INV_0 : INV
    port map (
      I => Out3_int(0),
      O => Result(0)
    );
  Mcount_Out1_int_xor_0_11_INV_0 : INV
    port map (
      I => Out1_int(0),
      O => Result_0_1
    );

end Structure;

