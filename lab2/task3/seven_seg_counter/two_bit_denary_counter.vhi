
-- VHDL Instantiation Created from source file two_bit_denary_counter.vhd -- 14:35:04 01/25/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT two_bit_denary_counter
	PORT(
		clock : IN std_logic;
		reset : IN std_logic;
		enable : IN std_logic;          
		Q_tens : OUT std_logic_vector(3 downto 0);
		Q_units : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_two_bit_denary_counter: two_bit_denary_counter PORT MAP(
		clock => ,
		reset => ,
		enable => ,
		Q_tens => ,
		Q_units => 
	);


