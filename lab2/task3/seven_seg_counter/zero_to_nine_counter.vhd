library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;



entity zero_to_nine_counter is
    Port ( clock : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           Q : out  UNSIGNED (3 downto 0);
           Cout : out  STD_LOGIC);
end zero_to_nine_counter;

architecture arch of zero_to_nine_counter is
	signal Q_int :UNSIGNED (3 downto 0);
begin
	count: process
		begin
			wait until (rising_edge(clock));
			if (reset = '1') then
				Q_int <= "0000";
				Cout <= '0';
			else 
				if( enable ='1') then
					if(Q_int = 9) then
						Q_int <="0000";
						Cout <='1';
					else 
						Q_int <= (Q_int +1);
						Cout <='0';
					end if;
				end if;
			end if;
	end process count;

Q <= Q_int;
end arch;

