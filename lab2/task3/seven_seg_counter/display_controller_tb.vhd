
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 

USE ieee.numeric_std.ALL;
 
ENTITY display_controller_tb IS
END display_controller_tb;
 
ARCHITECTURE behavior OF display_controller_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT display_controller
    PORT(
         clock : IN  std_logic;
         b_rst : IN  std_logic;
         b_cnt : IN  std_logic;
         Qtens : OUT  UNSIGNED(6 downto 0);
         Qunits : OUT  UNSIGNED(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal b_rst : std_logic := '0';
   signal b_cnt : std_logic := '0';

 	--Outputs
   signal Qtens : UNSIGNED(6 downto 0);
   signal Qunits : UNSIGNED(6 downto 0);

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: display_controller PORT MAP (
          clock => clock,
          b_rst => b_rst,
          b_cnt => b_cnt,
          Qtens => Qtens,
          Qunits => Qunits
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 
 -- simulate the user pressing of the button repeatedly
  enable_process :process
   begin
		b_cnt <= '0';
		wait for clock_period*2;
		b_cnt <= '1';
		wait for clock_period*2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		b_rst <= '1';
      wait for clock_period*2;
		b_rst <= '0';
      wait for clock_period*20;
		-- Reset the counter after counting for a while
		b_rst <= '1';
      wait for clock_period*2;
		b_rst <= '0';
      wait for clock_period*2;


      wait;
   end process;

END;
