library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity two_bit_denary_counter is
    Port ( clock : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           Q_tens : out  UNSIGNED (3 downto 0);
           Q_units : out  UNSIGNED (3 downto 0));
end two_bit_denary_counter;

architecture arch of two_bit_denary_counter is

	signal Q_units_int :UNSIGNED (3 downto 0);
	signal Q_tens_int :UNSIGNED (3 downto 0);
	
begin
	counter_units: process
		begin
			wait until (rising_edge(clock));
			if (reset = '1') then
				Q_units_int <= "0000";
			else 
				if(enable ='1') then
					if(Q_units_int = 9) then
						Q_units_int <="0000";
					else 
						Q_units_int <= (Q_units_int +1);
					end if;
				end if;
			end if;
	end process counter_units;


	counter_tens: process
		begin
			wait until (rising_edge(clock));
			if (reset = '1') then
				Q_tens_int <= "0000";
				
			else 
				--Essentially enable if other counter is 0
				if( enable ='1') then
					if( Q_units_int = 9) then
						if(Q_tens_int = 9) then
							Q_tens_int <="0000";
						else 
							Q_tens_int <= (Q_tens_int +1);						
						end if;
					end if;
				end if;
			end if;
	end process counter_tens;

	Q_tens <= Q_tens_int;
	Q_units <= Q_units_int;
end arch;

