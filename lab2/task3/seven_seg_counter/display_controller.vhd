library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity display_controller is
    Port ( clock : in  STD_LOGIC;
           b_rst : in  STD_LOGIC;
           b_cnt : in  STD_LOGIC;
           Qtens : out  UNSIGNED (6 downto 0);
           Qunits : out  UNSIGNED (6 downto 0));
end display_controller;

architecture Structural of display_controller is
	COMPONENT two_bit_denary_counter
	PORT(
		clock : IN std_logic;
		reset : IN std_logic;
		enable : IN std_logic;          
		Q_tens : OUT UNSIGNED(3 downto 0);
		Q_units : OUT UNSIGNED(3 downto 0)
		);
	END COMPONENT;

	COMPONENT Debouncer
	PORT(
		clk : IN std_logic;
		Sig : IN std_logic;          
		Deb_Sig : OUT std_logic
		);
	END COMPONENT;

	COMPONENT seven_seg_decoder
	PORT(
		BCDin : IN UNSIGNED(3 downto 0);          
		Qout : OUT UNSIGNED(6 downto 0)
		);
	END COMPONENT;
	
	signal Deb_rst: STD_LOGIC;
	signal Deb_cnt: STD_LOGIC;
	signal Qtens_int: UNSIGNED(3 downto 0);
	signal Qunits_int: UNSIGNED(3 downto 0);
	
begin
	deb0: Debouncer 
		PORT MAP (clk => clock, Sig =>b_rst, Deb_Sig =>Deb_rst);
	deb1: Debouncer 
		PORT MAP (clk => clock, Sig =>b_cnt, Deb_Sig =>Deb_cnt);
	dec0: seven_seg_decoder
		PORT MAP (BCDin =>Qtens_int, Qout =>Qtens);
	dec1: seven_seg_decoder
		PORT MAP (BCDin =>Qunits_int, Qout =>Qunits);
		
	counter0: two_bit_denary_counter
		PORT MAP (clock => clock, reset => Deb_rst, enable => Deb_cnt,    
		Q_tens =>Qtens_int,  Q_units =>Qunits_int);
		
		
		end Structural;

