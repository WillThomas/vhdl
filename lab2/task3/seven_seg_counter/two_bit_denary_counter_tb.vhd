
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY two_bit_denary_counter_tb IS
END two_bit_denary_counter_tb;
 
ARCHITECTURE behavior OF two_bit_denary_counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT two_bit_denary_counter
    PORT(
         clock : IN  std_logic;
         reset : IN  std_logic;
         enable : IN  std_logic;
         Q_tens : OUT  UNSIGNED(3 downto 0);
         Q_units : OUT  UNSIGNED(3 downto 0)
        
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal reset : std_logic := '0';
   signal enable : std_logic := '0';

 	--Outputs
   signal Q_tens : UNSIGNED(3 downto 0);
   signal Q_units : UNSIGNED(3 downto 0);


   -- Clock period definitions
   constant clock_period : time := 5 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: two_bit_denary_counter PORT MAP (
          clock => clock,
          reset => reset,
          enable => enable,
          Q_tens => Q_tens,
          Q_units => Q_units
  
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/4;
		clock <= '1';
		wait for clock_period/4;
   end process;
	

 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clock_period*10;

      -- insert stimulus here 

		reset <= '1';
		enable <='1';
		wait for clock_period *2;
		reset <= '0';
      wait;
   end process;

END;
