library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity seven_seg_decoder is
    Port ( BCDin : in  UNSIGNED (3 downto 0);
           Qout : out  UNSIGNED (6 downto 0));
end seven_seg_decoder;

architecture Behavioral of seven_seg_decoder is

begin
Qout <= 	"1111110" when BCDin = "0000" else
			"0110000" when BCDin = "0001" else
			"1101101" when BCDin = "0010" else
			"1111001" when BCDin = "0011" else
			"0110011" when BCDin = "0100" else
			"1011011" when BCDin = "0101" else
			"1011111" when BCDin = "0110" else
			"1110000" when BCDin = "0111" else
			"1111111" when BCDin = "1000" else
			"1110011" when BCDin = "1001" else
			"UUUUUUU";
	

end Behavioral;

