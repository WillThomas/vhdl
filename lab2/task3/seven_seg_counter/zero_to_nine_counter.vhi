
-- VHDL Instantiation Created from source file zero_to_nine_counter.vhd -- 19:48:28 01/22/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT zero_to_nine_counter
	PORT(
		clock : IN std_logic;
		reset : IN std_logic;
		enable : IN std_logic;          
		Q : OUT std_logic_vector(3 downto 0);
		Cout : OUT std_logic
		);
	END COMPONENT;

	Inst_zero_to_nine_counter: zero_to_nine_counter PORT MAP(
		clock => ,
		reset => ,
		enable => ,
		Q => ,
		Cout => 
	);


