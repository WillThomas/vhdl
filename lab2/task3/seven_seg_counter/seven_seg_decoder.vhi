
-- VHDL Instantiation Created from source file seven_seg_decoder.vhd -- 14:36:30 01/25/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT seven_seg_decoder
	PORT(
		BCDin : IN std_logic_vector(3 downto 0);          
		Qout : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;

	Inst_seven_seg_decoder: seven_seg_decoder PORT MAP(
		BCDin => ,
		Qout => 
	);


