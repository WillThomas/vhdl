
-- VHDL Instantiation Created from source file four_bit_adder.vhd -- 14:50:44 01/21/2014
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT four_bit_adder
	PORT(
		A : IN std_logic_vector(3 downto 0);
		B : IN std_logic_vector(3 downto 0);
		Cin : IN std_logic;          
		S : OUT std_logic_vector(3 downto 0);
		Cout : OUT std_logic
		);
	END COMPONENT;

	Inst_four_bit_adder: four_bit_adder PORT MAP(
		A => ,
		B => ,
		Cin => ,
		S => ,
		Cout => 
	);


